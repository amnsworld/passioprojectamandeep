namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addeddatetoworkorder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkOrders", "OrderDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WorkOrders", "OrderDate");
        }
    }
}
