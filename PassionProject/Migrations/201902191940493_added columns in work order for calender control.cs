namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcolumnsinworkorderforcalendercontrol : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkOrders", "StartDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.WorkOrders", "FinishDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.WorkOrders", "OrderDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.WorkOrders", "OrderDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.WorkOrders", "FinishDate");
            DropColumn("dbo.WorkOrders", "StartDate");
        }
    }
}
