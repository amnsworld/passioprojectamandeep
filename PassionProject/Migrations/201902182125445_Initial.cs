namespace PassionProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Laptops",
                c => new
                    {
                        LaptopID = c.Int(nullable: false, identity: true),
                        LaptopManufacturer = c.String(nullable: false, maxLength: 255),
                        LaptopModel = c.String(nullable: false, maxLength: 255),
                        LaptopProblem = c.String(),
                        Workorder_WorkOrderID = c.Int(),
                    })
                .PrimaryKey(t => t.LaptopID)
                .ForeignKey("dbo.WorkOrders", t => t.Workorder_WorkOrderID)
                .Index(t => t.Workorder_WorkOrderID);
            
            CreateTable(
                "dbo.WorkOrders",
                c => new
                    {
                        WorkOrderID = c.Int(nullable: false, identity: true),
                        WorkOrderName = c.String(nullable: false, maxLength: 255),
                        WorkOrderCustomerName = c.String(nullable: false, maxLength: 255),
                        WorkOrderPrice = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.WorkOrderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Laptops", "Workorder_WorkOrderID", "dbo.WorkOrders");
            DropIndex("dbo.Laptops", new[] { "Workorder_WorkOrderID" });
            DropTable("dbo.WorkOrders");
            DropTable("dbo.Laptops");
        }
    }
}
