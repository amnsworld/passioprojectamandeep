﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace PassionProject.Models
{
    public class Laptops
    {
        //Referred from sample project of Christine

        [Key, ScaffoldColumn(false)]
        public int LaptopID { get; set; }

        //required string length up to 255 characters,
        //labelled as "Manufacturer"
        [Required, StringLength(255), Display(Name = "Manufacturer")]
        public string LaptopManufacturer { get; set; }

        //Laptop Model
        [Required, StringLength(255), Display(Name = "Model")]
        public string LaptopModel { get; set; }

        //Laptop Problem
        [StringLength(int.MaxValue), Display(Name = "Problem")]
        public string LaptopProblem { get; set; }

        //One workorder to many laptops
        public virtual WorkOrder Workorder { get; set; }
    }
}