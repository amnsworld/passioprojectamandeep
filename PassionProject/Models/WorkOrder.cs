﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class WorkOrder
    {
        [Key]
        public int WorkOrderID { get; set; }

        [Required, StringLength(255), Display(Name = "Work Order Name")]
        public string WorkOrderName { get; set; }

        [Required, StringLength(255), Display(Name = "Customer Name")]
        public string WorkOrderCustomerName { get; set; }

        [Required, Display(Name = "Price")]
        [DataType(DataType.Currency)]
        public float WorkOrderPrice { get; set; }


        //reffered https://www.c-sharpcorner.com/article/displaying-calender-control-in-Asp-Net-mvc-without-jquery-an/ for calender control
        [Required,Display(Name ="Start Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? StartDate { get; set; }

        [Required, Display(Name = "Expected Finish Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? FinishDate { get; set; }

        //One workorder can have many laptops associated with it.
        public virtual ICollection<Laptops> Laptops { get; set; }
    }
}