﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace PassionProject.Models
{
    public class LaptopRepairDbContext : DbContext
    {
        public DbSet<Laptops> Laptops { get; set; }
        public DbSet<WorkOrder> WorkOrders { get; set; }
    }
}