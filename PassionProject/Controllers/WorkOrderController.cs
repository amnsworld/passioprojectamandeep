﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using PassionProject.ViewModels;

namespace PassionProject.Controllers
{
    public class WorkOrderController : Controller
    {
        //Refferenced code from the sample project done by Christine

        private LaptopRepairDbContext db = new LaptopRepairDbContext();
        // GET: WorkOrder
        public ActionResult Index()
        {
            return View(db.WorkOrders.ToList());
        }

        public ActionResult Create()
        {            
            return View();
        }

        //Restricts this method to only handle POST
        //eg. POST to /Pages/Create/
        [HttpPost]
        public ActionResult Create(string WorkOrderName_New, string WorkOrderCustomerName_New, float? WorkOrderPrice_New,DateTime? WorkOrderStartDate_New,DateTime? WorkOrderFinishDate_New)
        {
            string query = "insert into WorkOrders (WorkOrderName, WorkOrderCustomerName, WorkOrderPrice,StartDate,FinishDate) " +
                "values (@WorkOrderName,@WorkOrderCustomerName,@WorkOrderPrice,@StartDate,@FinishDate)";

            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@WorkOrderName", WorkOrderName_New);
            myparams[1] = new SqlParameter("@WorkOrderCustomerName", WorkOrderCustomerName_New);
            myparams[2] = new SqlParameter("@WorkOrderPrice", WorkOrderPrice_New);
            myparams[3] = new SqlParameter("@StartDate", WorkOrderStartDate_New);
            myparams[4] = new SqlParameter("@FinishDate", WorkOrderFinishDate_New);

            db.Database.ExecuteSqlCommand(query, myparams);
            
            return RedirectToAction("Index");
        }

        public ActionResult Details(int id)
        {
            string query = "select * from WorkOrders where WorkOrderID =@id";
            return View(db.WorkOrders.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        public ActionResult Edit(int? id)
        {
            WorkOrderViewModel workOrderViewModel = new WorkOrderViewModel();
            workOrderViewModel.workOrder = db.WorkOrders.Find(id);
            workOrderViewModel.laptops = db.Laptops.ToList();
            if (workOrderViewModel.workOrder != null)
                return View(workOrderViewModel);
            else
                return HttpNotFound();
        }

        //This one actually does the editing commmand
        [HttpPost, ActionName("Edit")]
        public ActionResult Edit(int? id, string WorkOrderName_Edit, string WorkOrderCustomerName_Edit, int? WorkOrderPrice_Edit, DateTime? WorkOrderStartDate_Edit, DateTime? WorkOrderFinishDate_Edit)
        {
            if ((id == null) || (db.WorkOrders.Find(id) == null))
            {
                return HttpNotFound();

            }
            /*enter data for page*/
            string query = "update WorkOrders set WorkOrderName=@WorkOrderName, " +
                "WorkOrderCustomerName=@WorkOrderCustomerName, " +
                "WorkOrderPrice=@WorkOrderPrice, " +
                "StartDate=@StartDate, " +
                "FinishDate=@FinishDate where WorkOrderID = @WorkOrderID";

            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@WorkOrderName", WorkOrderName_Edit);
            myparams[1] = new SqlParameter("@WorkOrderCustomerName", WorkOrderCustomerName_Edit);
            myparams[2] = new SqlParameter("@WorkOrderPrice", WorkOrderPrice_Edit);
            myparams[3] = new SqlParameter("@StartDate", WorkOrderStartDate_Edit);
            myparams[4] = new SqlParameter("@FinishDate", WorkOrderFinishDate_Edit);
            myparams[5] = new SqlParameter("@WorkOrderID", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Details/" + id);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkOrder workOrder= db.WorkOrders.Find(id);
            if (workOrder == null)
            {
                return HttpNotFound();
            }
            return View(workOrder);
        }

        //GET of localhost/WorkOrder/delete/2
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string query = "delete from Laptops where Workorder_WorkOrderID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));


            query = "delete from Workorders where WorkOrderID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));


            return RedirectToAction("Index");
        }

    }
}