﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PassionProject.Models;
using PassionProject.ViewModels;

namespace PassionProject.Controllers
{
    public class LaptopController : Controller
    {
        private LaptopRepairDbContext db = new LaptopRepairDbContext();
        // GET: Laptop
        public ActionResult Index()
        {            
            return View(db.Laptops.ToList());
        }

        public ActionResult Create()
        {
            LaptopRepairViewModel laptopRepairViewModel = new LaptopRepairViewModel();
            laptopRepairViewModel.workOrders = db.WorkOrders.ToList();
            return View(laptopRepairViewModel);

        }

        //Restricts this method to only handle POST
        //eg. POST to /Pages/Create/
        [HttpPost, ActionName("Create")]
        public ActionResult Create(string LaptopManufacturer_New, string LaptopModel_New, string LaptopProblem_New, int? LaptopWorkOrder_New)
        {
            string query = "insert into Laptops (LaptopManufacturer, LaptopModel, LaptopProblem,Workorder_WorkOrderID) " +
                "values (@LaptopManufacturer,@LaptopModel,@LaptopProblem,@Workorder_WorkOrderID)";

            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@LaptopManufacturer", LaptopManufacturer_New);
            myparams[1] = new SqlParameter("@LaptopModel", LaptopModel_New);
            myparams[2] = new SqlParameter("@LaptopProblem", LaptopProblem_New);
            myparams[3] = new SqlParameter("@Workorder_WorkOrderID", LaptopWorkOrder_New);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }


        public ActionResult Edit(int? id)
        {
            LaptopRepairViewModel laptopRepairViewModel = new LaptopRepairViewModel();
            laptopRepairViewModel.Laptops = db.Laptops.Find(id);
            laptopRepairViewModel.workOrders = db.WorkOrders.ToList();
            if (laptopRepairViewModel.Laptops != null)
                return View(laptopRepairViewModel);
            else
                return HttpNotFound();
        }

        //This one actually does the editing commmand
        [HttpPost, ActionName("Edit")]
        public ActionResult Edit(int? id, string LaptopManufacturer_Edit, string LaptopModel_Edit, string LaptopProblem_Edit, int LaptopWorkOrder_Edit)
        {
            if ((id == null) || (db.Laptops.Find(id) == null))
            {
                return HttpNotFound();

            }
            /*enter data for page*/
            string query = "update Laptops set LaptopManufacturer=@LaptopManufacturer, " +
                "LaptopModel=@LaptopModel, " +
                "LaptopProblem=@LaptopProblem, " +
                "Workorder_WorkorderID=@Workorder where LaptopID = @LaptopID";

            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@LaptopManufacturer", LaptopManufacturer_Edit);
            myparams[1] = new SqlParameter("@LaptopModel", LaptopModel_Edit);
            myparams[2] = new SqlParameter("@LaptopProblem", LaptopProblem_Edit);
            myparams[3] = new SqlParameter("@Workorder", LaptopWorkOrder_Edit);
            myparams[4] = new SqlParameter("@LaptopID", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Details/" + id);
        }

        public ActionResult Details(int id)
        {
            string query = "select * from Laptops where LaptopID =@id";
            return View(db.Laptops.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Laptops laptops = db.Laptops.Find(id);

            if (laptops == null)
            {
                return HttpNotFound();
            }
            return View(laptops);
        }

        //GET of localhost/Laptop/delete/2
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            string query = "delete from Laptops where LaptopID = @id";
            db.Database.ExecuteSqlCommand(query, new SqlParameter("@id", id));

            return RedirectToAction("Index");
        }
    }
}