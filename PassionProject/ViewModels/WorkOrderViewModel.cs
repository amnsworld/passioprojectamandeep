﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PassionProject.Models;

namespace PassionProject.ViewModels
{
    public class WorkOrderViewModel
    {
        //Empty constructor
        public WorkOrderViewModel()
        {

        }

        //Raw page information (in Models/WorkOrder.cs)
        public virtual WorkOrder workOrder { get; set; }

        //need information about the different laptops this workorder COULD be
        //assigned to
        public IEnumerable<Laptops>  laptops { get; set; }
    }
}