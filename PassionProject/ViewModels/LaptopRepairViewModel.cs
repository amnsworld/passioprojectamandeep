﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PassionProject.Models;

namespace PassionProject.ViewModels
{
    public class LaptopRepairViewModel
    {
        public IEnumerable<WorkOrder> workOrders { get; set; }
        public virtual Laptops Laptops { get; set; }
    }
}